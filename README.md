# Consumer for AZAC Researcher System

---
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

the project can listen and consume rabbitmq later write postgresql.

## Dependencies

### Language

 - Python 3.9

### Libraries

 - pika
 - psycopg2

