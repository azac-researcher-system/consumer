from time import sleep
from modules.consume_queue import consume_queue

if (__name__ == "__main__"):
    while (True):
        try:
            consume_queue()
            sleep(5)
        except Exception as err:
            print(err)