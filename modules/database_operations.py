import psycopg2

from datetime import datetime
from modules.settings import SETTINGS


class database_operations():
    def __init__(self):
        self.conn = psycopg2.connect(dbname=SETTINGS['PG_DATABASENAME'],
                                     user=SETTINGS['PG_USER'],
                                     password=SETTINGS['PG_PASSWORD'],
                                     host=SETTINGS['PG_HOST'],
                                     port=SETTINGS['PG_PORT'])
        cur = self.conn.cursor()
        cur.execute('SELECT MAX(id) FROM crawler_post;')
        max_id = cur.fetchone()[0]
        print(max_id)
    def insert_table(self,title, slug, content, KeyWords):
        """
        :param slug: What source does the data come from? url
        :param BotType: which bot is running
        :param summary: summary information about the source content
        :param KeyWords: Keywords
        """
        cur = self.conn.cursor()
        cur.execute('SELECT MAX(id) FROM crawler_post;')
        max_id = cur.fetchone()[0]

        now = datetime.now()
        published_at = now.strftime("%Y-%m-%d %H:%M:%S")

        cur.execute(
            'INSERT INTO crawler_post ("id", "author_id", "title", "slug", "summary", "content", "published_at") VALUES (%s, %s, %s, %s, %s, %s, %s)',
            (int(max_id)+1,
             2, # author_id for database i write manual but it will change
             title, # title pgsql Column name
             slug, # slug Column name
             KeyWords, # summary Column name
             content, # content Column name
             published_at) # published_at Column name
        )
        self.conn.commit()
        cur.close()

        self.conn.close()

database_operations()