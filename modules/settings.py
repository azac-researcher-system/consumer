from os import environ

SETTINGS = {
    'PG_DATABASENAME': environ.get('PG_DATABASENAME','queryvs'),
    'PG_USER': environ.get('PG_USER','azx'),
    'PG_PASSWORD': environ.get('PG_PASSWORD', 'azx123'),
    'PG_HOST': environ.get('PG_HOST', '192.168.122.11'),
    'PG_PORT': environ.get('PG_PORT', '5432'),
    'RMQ_HOST': environ.get('RMQ_HOST', '192.168.122.11'),
    'RMQ_PORT': environ.get('RMQ_PORT', '5672'),
    'RMQ_USERNAME': environ.get('RMQ_USERNAME', 'queryvs'),
    'RMQ_PASSWORD': environ.get('RMQ_PASSWORD', 'queryvs'),
    'RMQ_QUEUE': environ.get('RMQ_QUEUE', 'data')
}