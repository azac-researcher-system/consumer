import json
import pika

from modules.database_operations import database_operations
from modules.settings import SETTINGS

from time import sleep

class consume_queue:
    def __init__(self):
        connection = pika.BlockingConnection(pika.ConnectionParameters(host=SETTINGS['RMQ_HOST'],
                                                                       credentials=pika.PlainCredentials(
                                                                           SETTINGS['RMQ_USERNAME'],
                                                                           SETTINGS['RMQ_PASSWORD'])
                                                                       ))

        channel = connection.channel()
        channel.basic_consume(SETTINGS['RMQ_QUEUE'], self.callback , auto_ack=True)
        channel.start_consuming()


    def callback(self, ch, method, properties, body):
        print(body)
        data = json.loads(body)
        Title = data['Title']
        slug = data['slug']
        Content = data['Content']
        KeyWord = data['KeyWord']
        BotType = data['BotType']
        print(Title)
        print(KeyWord, BotType)
        try:
            database_operations().insert_table(Title,
                                             slug,
                                             Content,
                                             KeyWord)
        except Exception as err:
            # i will rewrite queue
            pass

# result body :
        # b'{"Title": "linux init Google results",
        #    "slug": "https://www.google.com/search?q=linux init",
        #    "Summary": "init command in Linux with examples - GeeksforGeeks: https://www.geeksforgeeks.org/init-command-in-linux-with-examples/8.1. \\u0130lk \\u00f6nce init gelir -  Belgeler.org: http://www.belgeler.org/sag/sag_init_first.htmlInit process on UNIX and Linux systems - Tutorialspoint: https://www.tutorialspoint.com/init-process-on-unix-and-linux-systemsLinux Init - javatpoint: https://www.javatpoint.com/linux-initinit - Wikipedia: https://en.wikipedia.org/wiki/Initinit - ArchWiki - Arch Linux: https://wiki.archlinux.org/title/initinit: http://belgeler.gen.tr/man/man8/man8-init.htmlTake Command: Init - Linux.it: https://www.linux.it/~rubini/docs/init/init.html",
        #    "KeyWord": "linux init",
        #    "BotType": "Google"
        # }'

